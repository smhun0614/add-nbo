#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <netinet/in.h>

int main(int argc, char *argv[]){

	FILE *ap;
	FILE *bp;

	uint32_t a;
	uint32_t b;

	ap = fopen(argv[1], "rb");
	bp = fopen(argv[2], "rb");

	fread(&a, sizeof(uint32_t), 1, ap);
	fread(&b, sizeof(uint32_t), 1,  bp);

	a = ntohl(a);
	b = ntohl(b);

	printf("%d(0x%x) + %d(0x%x) = %d(0x%x)\n", a, a, b, b, a + b, a + b);

	return 0;
}
